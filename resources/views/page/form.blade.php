@extends('layout.master')
@section('title')
Form Pendaftaran
@endsection
@section('content')
<form action="/kirim" method="post">
    @csrf
    <h1>Buat Account Baru</h1>
    <p>Sign Up Form</p>
    <label>First Name</label><br>
    <input type="text" name="first_name"><br><br>
    <label>Last Name</label><br>
    <input type="text" name="last_name"><br><br>
    <label>Gender</label><br>
    <input type="radio" value="Male">
    <label for="Male">Male</label><br>
    <input type="radio" value="Female">
    <label for="Female">Female</label><br><br>
    <label>Nationality</label><br>
    <select><option>Indonesia</option></select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" id="vehicle1" value="Bahasa Indonesia">
    <label for="vehicle1">Bahasa Indonesia</label><br>
    <input type="checkbox" id="vehicle1" value="English">
    <label for="vehicle1">English</label><br>
    <input type="checkbox" id="vehicle1" value="Other">
    <label for="vehicle1">Other</label><br><br>
    <label>Bio</label><br>
    <textarea name="bio" rows="6" cols="30"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection