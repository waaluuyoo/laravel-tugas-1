<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::get('/register','AuthController@register');
Route::post('/kirim','AuthController@kirim');
Route::get('/data-table','IndexController@table');

//crud cash
Route::get('/cast/create','CastController@create');//untuk form
Route::post('/cast','CastController@store');//untuk save

//read
Route::get('/cast','CastController@index');//untuk tampil data
Route::get('/cast/{cast_id}','CastController@show');//route detail cast

//edit
Route::get('/cast/{cast_id}/edit','CastController@edit');//route untuk arah ke form edit
Route::put('/cast/{cast_id}','CastController@update');

//delete
Route::delete('/cast/{cast_id}','CastController@destroy');//route untuk delete cash

