<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register ()
    {
        return view('page.form');
    }

    public function kirim (Request $request)
    {
        //dd($request->all());
        $nm_depan=$request['first_name'];
        $nm_belakang=$request['last_name'];

        return view('page.welcome',compact('nm_depan','nm_belakang'));
    }
}
